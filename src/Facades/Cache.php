<?php

namespace Tankbar\Fiora\Facades;

class Cache {
    public static function remember(
        string $key,
        callable $fetcher,
        int $expiration,
        string $group = '',
        bool $force = false
    ): mixed {
        /**
         * @psalm-suppress MixedAssignment
         */
        $value = wp_cache_get( $key, $group, $force, $foundValue );
        if ( ! $foundValue ) {
            /**
             * @psalm-suppress MixedAssignment
             */
            $value = $fetcher();

            wp_cache_set( $key, $value, $group, $expiration );
        }

        return $value;
    }
}
