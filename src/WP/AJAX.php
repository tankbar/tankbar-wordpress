<?php

namespace Tankbar\Fiora\WP;

class AJAX
{
    private static $instance = null;

    private static function instance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function listen(): void
    {
        add_action('template_redirect', fn() => static::instance()->check_for_request());
    }

    public function check_for_request(): void
    {
        $action = isset($_GET['t-ajax'])
            ? sanitize_text_field(wp_unslash($_GET['t-ajax']))
            : '';

        if (!empty($action)) {
            send_origin_headers();
            send_nosniff_header();

            header('Content-Type: text/html; charset=' . get_option('blog_charset'));
            header('X-Robots-Tag: noindex');

            $no_cache_headers = wp_get_nocache_headers();

            unset($no_cache_headers['Last-Modified']);

            header_remove('Last-Modified');

            foreach ($no_cache_headers as $key => $value) {
                header("{$key}: {$value}");
            }

            status_header(200);

            do_action("t_ajax_$action");
            wp_die();
        }
    }
}
