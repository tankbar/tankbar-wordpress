<?php

namespace Tankbar\Fiora;

use Dotenv\Dotenv;

class Env
{
    /**
     * Loads environment variables into memory.
     *
     * @param string|string[] $dir
     * @param string|string[]|null $names
     */
    public static function load($dir, $names = null): void
    {
        $env = Dotenv::createImmutable($dir, $names);
        $env->load();
    }
}
