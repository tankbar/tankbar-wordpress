<?php

namespace Tankbar\Fiora\Support;

class Arr
{
    public static function map(array $array, callable $callback): array
    {
        $result = [];
        /**
         * @psalm-suppress MixedAssignment
         */
        foreach ($array as $key => $value) {
            $result[] = $callback($value, $key);
        }

        return $result;
    }
}
