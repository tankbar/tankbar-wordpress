<?php

namespace Tankbar\Fiora\Support;

class Str
{
    /**
     * @param string[]|string $char
     * @param string[] $strings
     *
     * @return string[]
     */
    public static function wrap(array|string $char, array $strings): array
    {
        $one = !is_array($char) ? $char : $char[0];
        $two = !is_array($char) ? $char : $char[1];

        return array_map(
            function ($string) use ($one, $two) {
                return "$one$string$two";
            },
            $strings
        );
    }
}
