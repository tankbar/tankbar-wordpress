<?php

namespace Tankbar\Fiora;

use Sentry\State\Scope;
use function Sentry\configureScope;

/**
 * Class Sentry
 *
 * @package Tankbar
 */
class Sentry
{
    /**
     * @var ?Sentry $instance
     */
    private static $instance = null;

    private function __construct()
    {
    }

    /**
     * @param string $dsn The DSN to use.
     * @param string $version The version to use.
     * @param array<string, mixed> $options A list of options.
     */
    public static function install(string $dsn, string $version, array $options = array()): void
    {
        $instance = self::get_instance();

        add_action('wp', [$instance, 'configure_potential_user']);
        add_action('after_setup_theme', [$instance, 'configure_plugins']);

        $args = array_merge(
            [
                'dsn' => $dsn,
                'environment' => wp_get_environment_type(),
                'release' => wp_get_environment_type() === 'production' ? $version : null,
                'sample_rate' => wp_get_environment_type() === 'production'
                    ? 0.05
                    : 1.0,
                'traces_sample_rate' => wp_get_environment_type() === 'production'
                    ? 0.05
                    : 1.0,
            ],
            $options
        );

        \Sentry\init($args);
    }

    private static function get_instance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function configure_potential_user(): void
    {
        $user = wp_get_current_user();

        if ($user->exists()) {
            configureScope(function (Scope $scope) use ($user) {
                $scope->setUser(
                    array(
                        'id' => $user->ID,
                        'username' => $user->user_login,
                        'email' => $user->user_email,
                    )
                );
            });
        }
    }

    public function configure_plugins(): void
    {
        configureScope(function (Scope $scope) {
            if (!function_exists('get_plugins')) {
                require_once ABSPATH . 'wp-admin/includes/plugin.php';
            }

            $plugins = get_plugins();

            if (!empty($plugins)) {
                /**
                 * @var array<string, string> $transformed_plugins
                 */
                $transformed_plugins = array_reduce(
                    $plugins,
                    /**
                     * @param array<string, string> $carry
                     * @param array{ Name: string, Version: string } $plugin
                     */
                    function (array $carry, array $plugin) {
                        $carry[$plugin['Name']] = $plugin['Version'];

                        return $carry;
                    },
                    []
                );

                $scope->setContext(
                    'Plugins',
                    $transformed_plugins
                );
            }
        });
    }
}
