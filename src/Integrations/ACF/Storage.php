<?php
/**
 * Modified ACF functionality for Tankbar templates
 *
 * @package tankbar
 */

namespace Tankbar\Fiora\Integrations\ACF;

class Storage
{
    /**
     * The base path for storing ACF sync files.
     *
     * @var string
     */
    private string $acf_storage_url;

    /**
     * Kilamobler_ACF_Storage constructor.
     *
     * @param string $path
     */
    private function __construct(string $path)
    {
        $this->acf_storage_url = $path;
    }

    private function hook(): void
    {
        add_filter('acf/settings/show_admin', fn() => $this->show_admin());
        add_filter('acf/settings/save_json', fn() => $this->save_json());
        add_filter('acf/settings/load_json', fn($paths) => $this->load_json($paths));
    }

    /**
     * Installs this class.
     *
     * @param string $path
     */
    public static function install(string $path): void
    {
        (new self($path))
            ->hook();
    }

    /**
     * Makes sure only users with manage_options can manage ACF fields.
     *
     * @return bool
     */
    private function show_admin(): bool
    {
        return current_user_can('manage_options');
    }

    /**
     * Changes the url where ACF should store sync files.
     *
     * @return string
     */
    public function save_json(): string
    {
        return $this->acf_storage_url;
    }

    /**
     * Changes the location where ACF will store sync files.
     *
     * @param string[] $paths The current paths to store sync files for ACF.
     *
     * @return string[]
     */
    public function load_json(array $paths): array
    {
        array_splice($paths, 0, 1, $this->acf_storage_url);
        return $paths;
    }
}
