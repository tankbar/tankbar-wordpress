<?php

namespace Tankbar\Fiora;

function getEnvironment(): string
{
    return function_exists('wp_get_environment_type')
        ? wp_get_environment_type()
        : 'production';
}

function isScriptDebug(): bool
{
    return defined('SCRIPT_DEBUG') && SCRIPT_DEBUG;
}

function parseAssetSource(string $src, string $environment, bool $debug): string
{
	if (str_ends_with($src, '!')) {
		return substr($src, 0, strlen($src) - 1);
	}

    if (!str_starts_with($src, 'http') && !str_starts_with($src, '//')) {
        $src = get_theme_file_uri($src);
    }

    $info = pathinfo($src);

    if (!isset($info['extension'])) {
        return $src;
    }

    $suffix = ".{$info['extension']}";

    if (!$debug && $environment === 'production') {
        $suffix = ".min.{$info['extension']}";
    }

    if ($info['dirname'] === '/') {
        return str_replace('//', '/', "{$info['dirname']}/{$info['filename']}{$suffix}");
    } else {
        if ($info['dirname'] === '.') {
            return "{$info['filename']}{$suffix}";
        }
    }

    return "{$info['dirname']}/{$info['filename']}{$suffix}";
}
