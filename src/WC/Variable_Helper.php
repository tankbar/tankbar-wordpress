<?php
/**
 * Helpers for variable products.
 *
 * @package kilamobler
 */

namespace Tankbar\Fiora\WC;

/**
 * Class Kilamobler_WC_Product_Variable_Helper
 */
class Variable_Helper {
    /**
     * Gets the default variation of a variable product.
     *
     * @psalm-suppress UndefinedDocblockClass
     * @psalm-suppress MixedInferredReturnType
     *
     * @param \WC_Product_Variable $product The product.
     *
     * @return \WC_Product_Variation|null
     */
    public static function get_default_variation( $product ) {
        /**
         * A list of default attributes.
         *
         * @var array<string, string> $default_attributes
         */
        $default_attributes = $product->get_default_attributes();
        foreach ( $default_attributes as $default_attribute_key => $default_attribute_value ) {
            if ( strpos( $default_attribute_key, 'attribute_' ) === 0 ) {
                continue;
            }

            unset( $default_attributes[ $default_attribute_key ] );
            $default_attributes[ sprintf( 'attribute_%s', $default_attribute_key ) ] = $default_attribute_value;
        }

        try {
            /**
             * The data store.
             *
             * @psalm-suppress UndefinedDocblockClass
             * @psalm-suppress UndefinedClass
             *
             * @var \WC_Product_Data_Store_CPT $wc_data_store
             */
            $wc_data_store        = \WC_Data_Store::load( 'product' );
            /**
             * The default variation ID.
             *
             * @var int $default_variation_id
             */
            $default_variation_id = $wc_data_store->find_matching_product_variation( $product, $default_attributes );
        } catch ( \Exception $ex ) {
            $default_variation_id = 0;
        }

        if ( 0 !== $default_variation_id ) {
            /**
             * The default variation.
             *
             * @psalm-suppress UndefinedDocblockClass
             * @psalm-suppress UndefinedFunction
             *
             * @var \WC_Product|\WC_Product_Variation|null $default_variation
             */
            $default_variation = \wc_get_product( $default_variation_id );
            /**
             * @psalm-suppress UndefinedClass
             */
            if ( ! empty( $default_variation ) && $default_variation instanceof \WC_Product_Variation ) {
                return $default_variation;
            }
        }

        return null;
    }
}
