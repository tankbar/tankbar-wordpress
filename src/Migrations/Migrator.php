<?php

namespace Tankbar\Fiora\Migrations;

use Exception;
use WP_CLI;

class Migrator
{
    private string $path;

    public function __construct(string $path) {
        $this->path = $path;

        WP_CLI::add_command( 'migrator migrate', fn() => $this->migrate() );
        WP_CLI::add_command( 'migrator rollback', fn( $args ) => $this->rollback( $args ) );
        WP_CLI::add_command( 'migrator create', fn( array $args ) => $this->create( $args ) );
    }

    private function maybe_initialize(): void {
        global $wpdb;

        $migrations_table_exists = filter_var(
            $wpdb->get_var( "SELECT 1 FROM {$wpdb->prefix}migrator_migrations LIMIT 1" ),
            FILTER_VALIDATE_BOOLEAN
        );

        if ( ! $migrations_table_exists ) {
            $wpdb->query(
                "CREATE TABLE {$wpdb->prefix}migrator_migrations (id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT, migration VARCHAR(255))"
            );
        }
    }

    private function migrate(): void {
        $this->maybe_initialize();

        $migrations_to_apply = $this->get_migrations_to_apply(
            $this->get_migrations(),
            $this->get_applied_migrations()
        );

        foreach ( $migrations_to_apply as $migration ) {
            try {
                $this->apply_migration( $migration );

                WP_CLI::success(
                    sprintf(
                        'Applied %s',
                        pathinfo( basename( $migration ), PATHINFO_FILENAME )
                    )
                );
            } catch ( \Throwable $ex ) {
                WP_CLI::error(
                    sprintf(
                        "Failed to apply %s.\n%s",
                        pathinfo( basename( $migration ), PATHINFO_FILENAME ),
                        $ex->getMessage()
                    ),
                );
            }
        }

        if ( empty( $migrations_to_apply ) ) {
            WP_CLI::success( 'No migrations to apply' );
        }
    }

    private function rollback( $args ): void {
        $usage = 'wp migrator rollback [num]';

        $number_of_migrations_to_rollback = 1;
        if ( isset( $args[0] ) ) {
            $number_of_migrations_to_rollback = (int) $args[0];
            if ( $number_of_migrations_to_rollback < 1 ) {
                $number_of_migrations_to_rollback = 1;
            }
        }

        $this->maybe_initialize();

        $applied_migrations = $this->get_applied_migrations();

        $migrations_to_rollback = array_reverse(
            array_slice(
                $applied_migrations,
                -$number_of_migrations_to_rollback
            )
        );

        foreach ( $migrations_to_rollback as $applied_migration ) {
            try {
                $this->rollback_migration( $applied_migration );

                WP_CLI::success(
                    sprintf(
                        'Rolled back %s',
                        $applied_migration->name
                    )
                );
            } catch ( \Throwable $ex ) {
                WP_CLI::error(
                    sprintf(
                        "Failed to roll back %s.\n%s",
                        $applied_migration->name,
                        $ex->getMessage()
                    ),
                );
            }
        }

        if ( empty( $migrations_to_rollback ) ) {
            WP_CLI::success( 'No migrations to roll back' );
        }
    }

    private function create( array $args ): void {
        $usage = 'wp migrator create <name>';

        if ( empty( $args[0] ) ) {
            WP_CLI::log( 'Missing migration name' );
            WP_CLI::log( $usage );

            exit;
        }

        $date = new \DateTime();

        $name = sprintf( '%s_%s.php', $date->format( 'Y_m_d_H_i_s'), str_replace( '-', '_', sanitize_title( $args[0] ) ) );

        $contents = <<<'PHP'
<?php

use Tankbar\Fiora\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        
    }
};

PHP;

        if ( ! file_exists( $this->path ) ) {
            mkdir( $this->path );
        }

        $file = sprintf( '%s%s%s', $this->path, DIRECTORY_SEPARATOR, $name );

        file_put_contents( $file, $contents );

        if ( ! file_exists( $file ) ) {
            WP_CLI::error( sprintf( "Could not create migration %s", $file ) );
        }

        WP_CLI::success( sprintf( "Created migration %s", $file ) );
    }

    private function get_migrations(): array {
        $migrations = [];

        $files = new \DirectoryIterator( $this->path );
        foreach ( $files as $file ) {
            if (
                $file->isDot() ||
                is_dir( $file->getFilename() ) ||
                'php' !== $file->getExtension()
            ) {
                continue;
            }

            $migrations[] = sprintf( '%s%s%s', $this->path, DIRECTORY_SEPARATOR, $file->getFilename() );
        }

        return $migrations;
    }

    private function get_applied_migrations(): array {
        global $wpdb;

        return array_map(
            function ( $migration ) {
                $migration->id = (int) $migration->id;

                return $migration;
            },
            $wpdb->get_results(
                "SELECT id, migration AS name FROM {$wpdb->prefix}migrator_migrations ORDER BY id ASC"
            )
        );
    }

    private function get_migrations_to_apply( $migrations, $applied_migrations ): array {
        $migrations_to_apply = [];
        foreach ( $migrations as $migration ) {
            $is_applied = false;
            foreach ( $applied_migrations as $applied_migration ) {
                if (sprintf( '%s.php', $applied_migration->name ) === basename( $migration )) {
                    $is_applied = true;
                    break;
                }
            }

            if ( ! $is_applied ) {
                $migrations_to_apply[] = $migration;
            }
        }

        return $migrations_to_apply;
    }

    private function apply_migration( string $migration ): void {
        /**
         * @var Migration $instance
         */
        $instance = include $migration;

        global $wpdb;

        $instance->up();

        if ( $wpdb->last_error ) {
            throw new Exception( $wpdb->last_error );
        } else {
            $wpdb->flush();

            $wpdb->query(
                $wpdb->prepare(
                    "INSERT INTO {$wpdb->prefix}migrator_migrations(migration) VALUES(%s)",
                    pathinfo( basename( $migration ), PATHINFO_FILENAME )
                )
            );

            if ( $wpdb->last_error ) {
                $top_level_error = new Exception( $wpdb->last_error );

                $wpdb->flush();

                $instance->down();

                $child_error = null;
                if ( $wpdb->last_error ) {
                    $child_error = new Exception( $wpdb->last_error, 0, $top_level_error );

                    $wpdb->flush();
                }

                if ( $child_error ) {
                    throw $child_error;
                }

                throw $top_level_error;
            }
        }
    }

    private function rollback_migration( $applied_migration ) {
        $migration = sprintf( '%s%s%s.php', $this->path, DIRECTORY_SEPARATOR, $applied_migration->name );
        /**
         * @var Migration $instance
         */
        $instance = include $migration;

        global $wpdb;

        $instance->down();

        if ( $wpdb->last_error ) {
            throw new Exception( $wpdb->last_error );
        } else {
            $wpdb->flush();

            $wpdb->query(
                $wpdb->prepare(
                    "DELETE FROM {$wpdb->prefix}migrator_migrations WHERE id = %d",
                    $applied_migration->id
                )
            );

            if ( $wpdb->last_error ) {
                $top_level_error = new Exception( $wpdb->last_error );

                $wpdb->flush();

                $instance->up();

                $child_error = null;
                if ( $wpdb->last_error ) {
                    $child_error = new Exception( $wpdb->last_error, 0, $top_level_error );

                    $wpdb->flush();
                }

                if ( $child_error ) {
                    throw $child_error;
                }

                throw $top_level_error;
            }
        }
    }
}
