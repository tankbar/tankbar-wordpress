<?php

namespace Tankbar\Fiora\Migrations;

use Exception;

abstract class Migration
{
    private object $db;

    public function __construct()
    {
        $this->db = $GLOBALS['wpdb'];
    }

    abstract public function up(): void;

    abstract public function down(): void;

    final protected function query($statement): void
    {
        $this->db->query($statement);

        if ($this->db->last_error) {
            throw new Exception($this->db->last_error);
        }
    }

    final protected function get_results($statement)
    {
        $results = $this->db->get_results($statement);

        if ($this->db->last_error) {
            throw new Exception($this->db->last_error);
        }

        return $results;
    }

    final protected function get_col($statement, $col = 0)
    {
        $results = $this->db->get_col($statement, $col);

        if ($this->db->last_error) {
            throw new Exception($this->db->last_error);
        }

        return $results;
    }

    final protected function get_row($statement, $output = OBJECT, $row = 0)
    {
        $results = $this->db->get_row($statement, $output, $row);

        if ($this->db->last_error) {
            throw new Exception($this->db->last_error);
        }

        return $results;
    }

    final protected function get_var($statement)
    {
        $results = $this->db->get_var($statement);

        if ($this->db->last_error) {
            throw new Exception($this->db->last_error);
        }

        return $results;
    }

    final protected function prepare($statement, ...$placeholders)
    {
        return $this->db->prepare($statement, ...$placeholders);
    }
}
