<?php
/**
 * Tankbar assets handling.
 *
 * @package Tankbar
 */

namespace Tankbar\Fiora;

/**
 * Class Assets
 *
 * @package Tankbar
 */
class Assets
{
    /**
     * Assets constructor.
     */
    private function __construct()
    {
    }

    public static function get_critical_css(string $critical_css_path): void {
        $critical_css_path = parseAssetSource($critical_css_path, getEnvironment(), isScriptDebug());

        if (file_exists($critical_css_path)) {
            ?>
            <style>
                <?php echo file_get_contents( $critical_css_path ); ?>
            </style>
            <?php
        }
    }

    /**
     * Enqueues script.
     *
     * @param string $handle The script handle.
     * @param string $src The script source.
     * @param array<string, string> $deps The script dependencies.
     * @param string $version The script version.
     * @param array{strategy: string, in_footer: bool}|bool $args Optional. An array of additional script loading strategies. Otherwise, it may be a boolean in which case it determines whether the script is printed in the footer. Default false.
     *
     * @return void
     */
    public static function enqueue_script(
        string $handle,
        string $src = '',
        array $deps = array(),
        string $version = '',
        $args = true
    ): void {
        if (!empty($src)) {
            self::register_script($handle, $src, $deps, $version, $args);
        }

        wp_enqueue_script($handle);
    }

    /**
     * Registers script.
     *
     * @param string $handle The script handle.
     * @param string $src The script source.
     * @param array<string, string> $deps The script dependencies.
     * @param string $version The script version.
     * @param array{strategy: string, in_footer: bool}|bool $args Optional. An array of additional script loading strategies. Otherwise, it may be a boolean in which case it determines whether the script is printed in the footer. Default false.
     *
     * @return void
     */
    public static function register_script(
        string $handle,
        string $src,
        array $deps,
        string $version,
        $args
    ): void {
        wp_register_script(
            $handle,
            parseAssetSource($src, getEnvironment(), isScriptDebug()),
            $deps,
            $version,
            $args
        );
    }

    public static function inline_script(string $handle, string $contents): void
    {
        wp_add_inline_script($handle, $contents);
    }

    /**
     * Enqueues style.
     *
     * @param string $handle The style handle.
     * @param string $src The style source.
     * @param array<string, string> $deps The style dependencies.
     * @param string $version The style version.
     * @param string $media The style media.
     *
     * @return void
     */
    public static function enqueue_style(
        string $handle,
        string $src = '',
        array $deps = array(),
        string $version = '',
        string $media = ''
    ): void {
        if (!empty($src)) {
            self::register_style($handle, $src, $deps, $version, $media);
        }

        wp_enqueue_style($handle);
    }

    /**
     * Registers style.
     *
     * @param string $handle The style handle.
     * @param string $src The style source.
     * @param array<string, string> $deps The style dependencies.
     * @param string $version The style version.
     * @param string $media The style media.
     *
     * @return void
     */
    public static function register_style(
        string $handle,
        string $src,
        array $deps,
        string $version,
        string $media
    ): void {
        wp_register_style($handle, parseAssetSource($src, getEnvironment(), isScriptDebug()), $deps, $version, $media);
    }
}
