<?php

use function Tankbar\Fiora\parseAssetSource;

function get_theme_file_uri(string $src): string
{
    if (str_starts_with($src, '/')) {
        $src = substr($src, 1);
    }

    return "https://example.com/wp-content/themes/example.com/$src";
}

final class FunctionsTest extends \PHPUnit\Framework\TestCase
{
    public function testParseAssetSource(): void
    {
        $this->assertEquals(
            'https://example.com/wp-content/themes/example.com/main.min.js',
            parseAssetSource('main.js', 'production', false),
            'Ensure that files get the minified file name if in production.'
        );

        $this->assertEquals(
            'https://example.com/wp-content/themes/example.com/main.js',
            parseAssetSource('main.js', 'development', false),
            'Ensure that file is not minified if not in production.'
        );

        $this->assertEquals(
            'https://example.com/wp-content/themes/example.com/gronk.js',
            parseAssetSource('/gronk.js', 'development', false),
            'Ensure no extra leading slashes.'
        );

        $this->assertEquals(
            'https://example.com/wp-content/themes/example.com/static/gronk.js',
            parseAssetSource('/static/gronk.js', 'development', false),
            'Ensure relative paths are prefixed with theme URI'
        );

        $this->assertEquals(
            'https://www.tankbar.com/static/gronk.js',
            parseAssetSource('https://www.tankbar.com/static/gronk.js', 'development', false),
            'Ensure full URLs work'
        );

	    $this->assertEquals(
		    '//tankbar.com/static/gronk.js',
		    parseAssetSource('//tankbar.com/static/gronk.js!', 'development', false),
		    'Ensure URLs are left unchanged if suffixed with !'
	    );

	    $this->assertEquals(
		    'https://www.tankbar.com/static/gronk.js',
		    parseAssetSource('https://www.tankbar.com/static/gronk.js!', 'development', false),
		    'Ensure URLs are left unchanged if suffixed with !'
	    );

	    $this->assertEquals(
		    '//tankbar.com/static/gronk.js',
		    parseAssetSource('//tankbar.com/static/gronk.js', 'development', false),
		    'Ensure URLs starting with // are handled as relative paths'
	    );

	    $this->assertEquals(
		    '//tankbar.com/static/gronk.js',
		    parseAssetSource('//tankbar.com/static/gronk.js', 'production', true),
		    'Ensure URLs starting with // are handled as relative paths'
	    );
    }
}
